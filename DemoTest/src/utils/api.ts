import axios from 'axios';

export async function get(url,  callback) {
  console.log(`url`, url);
  // console.log(`params`, params);
  try {
    const response = await axios.get(url);
    if (response.status === 200) {
      callback(response);
    }
  } catch (error) {
    console.error('Error', error);
  }
}

export async function post1(url, params, callback) {
  try {
    const options = {
      headers: { 'Content-Type': 'application/json' },
    };
    const response = await axios.post(url, params, options);
    if (response.status === 200) {
      callback(response);
    }
  } catch (error) {
    console.error('Error', error);
  }
}

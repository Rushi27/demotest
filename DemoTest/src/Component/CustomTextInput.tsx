import React, { useState } from 'react';
import { StyleSheet, TextInput, KeyboardType, View, Text } from 'react-native';

type Styles = {
  container?: any;
  input?: any;
};

type Props = {
  placeholder?: string;
  value: string;
  onChangeText?: any;
  secureTextEntry?: boolean;
  error?: boolean;
  customStyle?: Styles;
  maxLength?: number;
  autoFocus?: boolean;
  keyboardType?: KeyboardType;
  startAdornment?: any;
  endAdornment?: any;
  onSubmitEditing?: any;
  returnKeyType?: any;
  onFocus?: any;
  onBlur?: any;
  errMsg?: string;
  editable?: boolean;
};

export const CustomTextInput = React.forwardRef((props: Props, ref: any) => {
  const {
    placeholder,
    value,
    onChangeText,
    secureTextEntry,
    error,
    customStyle,
    maxLength,
    autoFocus,
    keyboardType,
    startAdornment,
    endAdornment,
    onSubmitEditing,
    returnKeyType,
    errMsg,
    editable,
  } = props;
  const [focused, setFocused] = useState<boolean>(false);

  return (
    <>
      <View
        style={
          customStyle?.container ?? {
            ...styles.container,
            ...(error
              ? styles.mistake
              : focused
              ? styles.focused
              : styles.normal),
          }
        }>
        {startAdornment}
        <TextInput
          {...props}
          ref={ref}
          style={
            customStyle?.input ?? {
              ...styles.input,
            }
          }
          editable={editable ?? true}
          placeholder={placeholder}
          autoCapitalize="none"
          value={value}
          placeholderTextColor={'#000'}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry ?? false}
          maxLength={maxLength}
          autoFocus={autoFocus ?? false}
          keyboardType={keyboardType}
          returnKeyType={returnKeyType}
          onSubmitEditing={onSubmitEditing}
        />

        {endAdornment}
      </View>
      <Text style={{ color: 'red' }}>{errMsg}</Text>
    </>
  );
});

const styles = StyleSheet.create({
  container: {
    width: '100%',
    borderStyle: 'solid',
    borderBottomWidth: 2,
    borderRadius: 4,
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 5,
    paddingVertical: 8,
    paddingHorizontal: 8,
  },
  input: {
    zIndex: 0,
    flex: 1,
    textAlign: 'left',
    padding: 0,
    color: '#000',
  },
  focused: {
    borderColor: 'grey',
  },
  normal: {
    borderColor: 'grey',
  },
  mistake: {
    borderColor: 'red',
  },
  right: {
    textAlign: 'right',
  },
});

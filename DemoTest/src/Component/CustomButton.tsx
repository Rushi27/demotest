import React from 'react';
import { StyleSheet, Text, ViewStyle, Button } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

type LongButtonProps = {
  style?: ViewStyle;
  title: string;
  onPress: () => void;
  disabled?: boolean;
};

export function CustomButton({
  style,
  title,
  onPress,
  disabled,
}: LongButtonProps) {
  return (
    <TouchableOpacity
      disabled={disabled}
      style={[styles.genericButton, style]}
      onPress={onPress}
      keyboardShouldPersistTaps={'always'}>
      {title ? (
        <Text style={{ fontSize: 18, fontWeight: '800', color: '#fff' }}>
          {title}
        </Text>
      ) : null}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  genericButton: {
    backgroundColor: 'rgb(255,68,32)',
    height: 45,
    width: '70%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    alignSelf: 'center',
    borderWidth: 2,
  },
});

/* eslint-disable no-alert */
import React, { useRef, useState } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { CustomTextInput } from '../Component/CustomTextInput';
import { CustomButton } from '../Component/CustomButton';
import auth from '@react-native-firebase/auth';
import { StackActions } from '@react-navigation/core';
import AsyncStorage from '@react-native-async-storage/async-storage';

const SignupScreen = ({ navigation }) => {
  const passwordRef = useRef(null);
  const [email, setEmail] = useState('eve.holt@reqres.in');
  const [password, setPassword] = useState('Test@123D');
  const [secureTextEntry] = useState(true);
  const [errorEmail, setErrorEmail] = useState(false);
  const [errorPassword, setErrorPassword] = useState(false);
  const [enable, setEnable] = useState(false);
  var re =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const signupFun = () => {
    if (email === '' || !re.test(email)) {
      setErrorEmail(true);
    } else if (password === '') {
      setErrorPassword(true);
    } else {
      setEnable(true);
      auth()
        .createUserWithEmailAndPassword(email, password)
        .then(async () => {
          await AsyncStorage.setItem('isLogin', JSON.stringify(true));
          navigation.dispatch(StackActions.replace('Home'));
        })
        .catch(error => {
          if (error.code === 'auth/email-already-in-use') {
            alert('That email address is already in use!');
          }

          if (error.code === 'auth/invalid-email') {
            alert('That email address is invalid!');
          }

          console.error(error);
        });
    }
  };

  return (
    <View style={styles.container}>
      <CustomTextInput
        placeholder="Email"
        value={email}
        onChangeText={text => setEmail(text)}
        error={errorEmail}
        onBlur={() => {
          if (email === '' || !re.test(email)) {
            setErrorEmail(true);
          } else {
            setErrorEmail(false);
          }
        }}
        errMsg={errorEmail ? 'Please Enter Email' : null}
        returnKeyType={'next'}
        onSubmitEditing={() => {
          passwordRef.current.focus();
        }}
      />

      <CustomTextInput
        ref={passwordRef}
        placeholder="Password"
        value={password}
        onChangeText={text => setPassword(text)}
        secureTextEntry={secureTextEntry}
        error={errorPassword}
        onBlur={() => {
          if (password === '') {
            setErrorPassword(true);
          } else {
            setErrorPassword(false);
          }
        }}
        errMsg={errorPassword ? 'Please Enter Password' : null}
        returnKeyType={'SIGNUP'}
      />

      <Text style={styles.textView}>
        Already have an account?
        <Text
          style={styles.textColor}
          onPress={() => {
            navigation.navigate('Login');
          }}>
          {' '}
          Login here
        </Text>
      </Text>
      <CustomButton onPress={signupFun} title="Sign Up" disabled={enable} />
    </View>
  );
};
export default SignupScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    marginTop: 30,
    paddingHorizontal: 20,
  },
  textView: {
    alignSelf: 'center',
  },
  textColor: {
    color: 'red',
  },
});

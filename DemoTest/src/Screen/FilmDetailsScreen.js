/* eslint-disable no-alert */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import FilmDetailsAction from '../Actions/FilmDetailsAction';
import { useDispatch } from 'react-redux';
import { useAppSelector } from '../utils/store';

const FilmDetailsScreen = ({ navigation, route }) => {
  const url = route.params.url;
  const dispatch = useDispatch();
  const [filmTitle, setFilmTitle] = useState();
  const [director, setDirector] = useState();
  const [producer, setProducer] = useState();
  const { isLoading, error, filmdetails } = useAppSelector(
    state => state.filmdetails,
  );

  useEffect(() => {
    dispatch(FilmDetailsAction(url));
  }, []);

  useEffect(() => {
    console.log('isLoading, error, filmdetails', isLoading, error, filmdetails);
    if (!isLoading) {
      if (error) {
        alert(error);
      }
      if (filmdetails) {
        setFilmTitle(filmdetails.title);
        setProducer(filmdetails.producer);
        setDirector(filmdetails.director);
      }
    }
  }, [isLoading, error, filmdetails]);

  return (
    <View style={style.container}>
      <Text style={style.titleText}>{filmTitle}</Text>
      <Text style={style.filmTitle}>Producer: {producer}</Text>
      <Text style={style.filmTitle}>Director: {director}</Text>
    </View>
  );
};
export default FilmDetailsScreen;

const style = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },
  titleText: {
    marginTop: 5,
    fontSize: 18,
    alignSelf: 'center',
  },
  filmTitle: {
    marginTop: 5,
    fontSize: 14,
  },
});

/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  FlatList,
  Alert,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import { useDispatch } from 'react-redux';
import PeopleAction from '../Actions/PeopleAction';
import Indicator from '../Component/Indicator';
import { useAppSelector } from '../utils/store';

const PeopleListScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const [peopleList, setPeopleList] = useState();
  const [page, setPage] = useState(1);
  const { isLoading, people, error } = useAppSelector(state => state.people);

  useEffect(() => {
    console.log('isLoading, error, people', isLoading, error, people);
    if (!isLoading) {
      if (error) {
        Alert.alert(error);
      }
      if (people) {
        setPeopleList(people);
        console.log('peopleList', peopleList);
      }
    }
  }, [isLoading, error, people]);

  useEffect(() => {
    dispatch(PeopleAction(page));
  }, []);

  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        style={style.cardView}
        onPress={() => {
          navigation.navigate('Person Details', { detail: item });
        }}
        key={index}>
        <Text>Name: {item.name}</Text>
        <Text>Height: {item.height}</Text>
        <Text>Birth Year: {item.birth_year}</Text>
        <Text>Gender: {item.gender}</Text>
      </TouchableOpacity>
    );
  };

  const onEndReached = () => {
    setPage(page + 1);
    console.log('Fetch More', page + 1);
    if (peopleList.length === 10) {
      dispatch(PeopleAction(page + 1));
    }
  };

  return (
    <View style={style.container}>
      {isLoading ? (
        <Indicator />
      ) : (
        <FlatList
          data={peopleList}
          extraData={peopleList}
          renderItem={renderItem}
          keyExtractor={({ index }) => index}
          onEndReachedThreshold={0}
          onEndReached={onEndReached}
        />
      )}
    </View>
  );
};
export default PeopleListScreen;

const style = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardView: {
    marginHorizontal: 15,
    marginTop: 15,
    shadowColor: 'rgba(0, 0, 0, 0.5)',
    shadowOffset: { x: 0, y: 10 },
    shadowOpacity: 4,
    backgroundColor: 'white',
    borderRadius: 15,
    padding: 15,
  },
});

/* eslint-disable no-alert */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { useDispatch } from 'react-redux';
import FilmAction from '../Actions/FilmAction';
import Indicator from '../Component/Indicator';
import { useAppSelector } from '../utils/store';

const FilmListScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const [filmList, setFilmList] = useState();

  const { isLoading, film, error } = useAppSelector(state => state.film);

  useEffect(() => {
    dispatch(FilmAction());
  }, []);

  useEffect(() => {
    if (!isLoading) {
      if (error) {
        alert(error);
      }
      if (film) {
        setFilmList(film);
      }
    }
  }, [isLoading, error, film]);

  const renderItem = ({ item, index }) => {
    console.log('item', item);
    return (
      <TouchableOpacity
        style={style.cardView}
        onPress={() => {
          navigation.navigate('Film Details', { url: item.url });
        }}>
        <Text style={style.titleText}>{item.title}</Text>
        <Text>Director: {item.director}</Text>
        <Text>Producer: {item.producer}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={style.container}>
      {isLoading ? (
        <Indicator />
      ) : (
        <FlatList
          data={filmList}
          renderItem={renderItem}
          keyExtractor={({ index }) => index}
        />
      )}
    </View>
  );
};
export default FilmListScreen;

const style = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardView: {
    marginHorizontal: 15,
    marginTop: 15,
    shadowColor: 'rgba(0, 0, 0, 0.5)',
    shadowOffset: { x: 0, y: 10 },
    shadowOpacity: 4,
    backgroundColor: 'white',
    borderRadius: 15,
    padding: 15,
  },
  titleText: {
    marginTop: 5,
    fontSize: 18,
    alignSelf: 'center',
  },
  filmTitle: {
    marginTop: 5,
    fontSize: 14,
  },
});

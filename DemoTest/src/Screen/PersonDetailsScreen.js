import React, { useState } from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
const PersonDetailsScreen = ({ navigation, route }) => {
  const personDetail = route.params && route.params.detail;
  console.log('object', personDetail);
  const renderItem = ({ item, index }) => {
    return (
      <View>
        <Text
          onPress={() => {
            navigation.navigate('Film Details', { url: item });
          }}>
          {item}
        </Text>
      </View>
    );
  };
  return (
    <View style={style.container}>
      <Text style={style.titleText}>{personDetail.name}</Text>
      <Text style={style.filmTitle}>Gender: {personDetail.gender}</Text>
      <Text style={style.filmTitle}>Birth Year: {personDetail.birth_year}</Text>
      <Text>{'\nFilm List\n'}</Text>
      <FlatList data={personDetail.films} renderItem={renderItem} />
    </View>
  );
};
export default PersonDetailsScreen;

const style = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },
  titleText: {
    marginTop: 5,
    fontSize: 25,
    alignSelf: 'center',
  },
  filmTitle: {
    marginTop: 5,
    fontSize: 14,
  },
});

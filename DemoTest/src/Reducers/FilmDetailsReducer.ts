export const FILM_DETAILS_FETCH = 'FILM_DETAILS_FETCH';
export const FILM_DETAILS_FETCH_SUCCESS = 'FILM_DETAILS_FETCH_SUCCESS';
export const FILM_DETAILS_FETCH_FAILURE = 'FILM_DETAILS_FETCH_FAILURE';

const initialState = {
  isLoading: false,
  filmdetails: '',
  error: '',
};

function FilmDetailsReducer(state = initialState, action) {
  switch (action.type) {
    case FILM_DETAILS_FETCH:
      return {
        isLoading: true,
        filmdetails: '',
        error: '',
      };
    case FILM_DETAILS_FETCH_SUCCESS:
      return {
        isLoading: false,
        filmdetails: action.filmdetails,
        error: '',
      };
    case FILM_DETAILS_FETCH_FAILURE:
      return {
        isLoading: false,
        filmdetails: '',
        error: action.error,
      };
    default:
      return state;
  }
}

export default FilmDetailsReducer;

export const PEOPLE_FETCH = 'PEOPLE_FETCH';
export const PEOPLE_FETCH_SUCCESS = 'PEOPLE_FETCH_SUCCESS';
export const PEOPLE_FETCH_FAILURE = 'PEOPLE_FETCH_FAILURE';

const initialState = {
  isLoading: false,
  people: [],
  error: '',
};

function PeopleReducer(state = initialState, action) {
  console.log('action', action);
  switch (action.type) {
    case PEOPLE_FETCH:
      return {
        isLoading: true,
        people: '',
        error: '',
      };
    case PEOPLE_FETCH_SUCCESS:
      const newArray  = [...state.people, ...action.people]
      console.log('newArray',  ...state.people)
      return {
        isLoading: false,
        error: '',
        people: newArray,
      };
    case PEOPLE_FETCH_FAILURE:
      return {
        isLoading: false,
        people: '',
        error: action.error,
      };
    default:
      return state;
  }
}

export default PeopleReducer;

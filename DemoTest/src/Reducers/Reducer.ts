import { combineReducers } from "redux";
import FilmDetailsReducer from "./FilmDetailsReducer";
import FilmReducer from "./FilmReducer";
import PeopleReducer from "./PeopleReducer";

export const Reducer = combineReducers({
    people: PeopleReducer,
    film: FilmReducer,
    filmdetails: FilmDetailsReducer,
});
export const FILM_FETCH = 'FILM_FETCH';
export const FILM_FETCH_SUCCESS = 'FILM_FETCH_SUCCESS';
export const FILM_FETCH_FAILURE = 'FILM_FETCH_FAILURE';

const initialState = {
  isLoading: false,
  film: '',
  error: '',
};

function FilmReducer(state = initialState, action) {
  console.log('object', action.film)
  switch (action.type) {
    case FILM_FETCH:
      return {
        isLoading: true,
        film: '',
        error: '',
      };
    case FILM_FETCH_SUCCESS:
      return {
        isLoading: false,
        film: action.film,
        error: '',
      };
    case FILM_FETCH_FAILURE:
      return {
        isLoading: false,
        film: '',
        error: action.error,
      };
    default:
      return state;
  }
}

export default FilmReducer;

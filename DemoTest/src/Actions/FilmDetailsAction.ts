
import { FILM_DETAILS_FETCH, FILM_DETAILS_FETCH_FAILURE, FILM_DETAILS_FETCH_SUCCESS } from "../Reducers/FilmDetailsReducer";
import { get } from "../utils/api";



function FilmDetailsFetch() {
  return {
    type: FILM_DETAILS_FETCH,
  };
}

function FilmDetailsFetchSuccess(filmdetails) {
  return {
    type: FILM_DETAILS_FETCH_SUCCESS,
    filmdetails,
  };
}

function FilmDetailsFetchFailure(error) {
  return {
    type: FILM_DETAILS_FETCH_FAILURE,
    error,
  };
}



const FilmDetailsAction = (url) => {
  return function (dispatch) {
    dispatch(FilmDetailsFetch());
    try {
      get(url, response => {
        console.log('object', response);
        if(response.status === 200){
            dispatch(FilmDetailsFetchSuccess(response.data))
        } else{
            dispatch(FilmDetailsFetchFailure(response.statusText)) 
        }
      });
    } catch (e: any) {
      console.log('{e}', { e });
    }
  };
};

export default FilmDetailsAction;

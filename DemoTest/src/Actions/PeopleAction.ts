import {
  PEOPLE_FETCH,
  PEOPLE_FETCH_FAILURE,
  PEOPLE_FETCH_SUCCESS,
} from '../Reducers/PeopleReducer';
import { get } from '../utils/api';
import { people_url } from '../utils/hosts';

function PeopleFetch() {
  return {
    type: PEOPLE_FETCH,
  };
}

function PeopleFetchSuccess(people) {
  return {
    type: PEOPLE_FETCH_SUCCESS,
    people,
  };
}

function PeopleFetchFailure(error) {
  return {
    type: PEOPLE_FETCH_FAILURE,
    error,
  };
}

const PeopleAction = page => {
  return function async(dispatch) {
    dispatch(PeopleFetch());
    try {
      get(people_url + page, response => {
        console.log('object', response);
        if (response.status === 200) {
          dispatch(PeopleFetchSuccess(response.data.results));
        } else {
          dispatch(PeopleFetchFailure(response.statusText));
        }
      });
    } catch (e: any) {
      console.log('{e}', { e });
    }
  };
};

export default PeopleAction;

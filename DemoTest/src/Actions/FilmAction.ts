
import { FILM_FETCH, FILM_FETCH_FAILURE, FILM_FETCH_SUCCESS } from "../Reducers/FilmReducer";
import { get } from "../utils/api";
import { films_url } from "../utils/hosts";


function FilmFetch() {
  return {
    type: FILM_FETCH,
  };
}

function FilmFetchSuccess(film) {
  return {
    type: FILM_FETCH_SUCCESS,
    film,
  };
}

function FilmFetchFailure(error) {
  return {
    type: FILM_FETCH_FAILURE,
    error,
  };
}



const FilmAction = () => {
  return function (dispatch) {
    dispatch(FilmFetch());
    try {
      get(films_url, response => {
        console.log('object', response);
        if(response.status === 200){
            dispatch(FilmFetchSuccess(response.data.results))
        } else{
            dispatch(FilmFetchFailure(response.statusText)) 
        }
      });
    } catch (e: any) {
      console.log('{e}', { e });
    }
  };
};

export default FilmAction;

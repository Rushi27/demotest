/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator, DrawerItem } from '@react-navigation/drawer';
import LoginScreen from './src/Screen/LoginScreen';
import { createStackNavigator } from '@react-navigation/stack';
import SignupScreen from './src/Screen/SignupScreen';
import PeopleListScreen from './src/Screen/PeopleListScreen';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Provider } from 'react-redux';
import store from './src/utils/store';
import FilmListScreen from './src/Screen/FilmListScreen';
import PersonDetailsScreen from './src/Screen/PersonDetailsScreen';
import PushNotification from 'react-native-push-notification';
import FilmDetailsScreen from './src/Screen/FilmDetailsScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';

const App = ({ navigation }) => {
  const [isLogin, setIsLogin] = useState();

  const init = async () => {
    const login = await AsyncStorage.getItem('isLogin');
    console.log('login', login);
    setIsLogin(login);
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      const token = await messaging().getToken();
      console.log('Authorization status:', authStatus, token);
    }
  };

  useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      PushNotification.createChannel({
        channelId: 'channel-id', // (required)
        channelName: 'DemoTestChannel', // (required)
      });
      PushNotification.localNotification({
        message: remoteMessage.notification.body,
        title: remoteMessage.notification.title,
        channelId: 'channel-id',
      });
    });

    return unsubscribe;
  }, []);

  useEffect(() => {
    init();
  }, []);

  const Drawer = createDrawerNavigator();
  const Stack = createStackNavigator();

  const PeopleContainer = () => {
    return (
      <Stack.Navigator screenOptions={{ headerTitleAlign: 'center' }}>
        <Stack.Screen
          name="People List"
          component={PeopleListScreen}
          options={({ navigation }) => ({
            headerLeft: () => (
              <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
                <Icon
                  name="bars"
                  size={25}
                  color="#D4AF37"
                  onPress={() => navigation.toggleDrawer()}
                />
              </TouchableOpacity>
            ),
            headerLeftContainerStyle: { paddingLeft: 10 },
          })}
        />
        <Stack.Screen name="Person Details" component={PersonDetailsScreen} />
        <Stack.Screen name="Film Details" component={FilmDetailsScreen} />
      </Stack.Navigator>
    );
  };

  const FilmContainer = () => {
    return (
      <Stack.Navigator screenOptions={{ headerTitleAlign: 'center' }}>
        <Stack.Screen
          name="Film List"
          component={FilmListScreen}
          options={({ navigation }) => ({
            headerLeft: () => (
              <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
                <Icon
                  name="bars"
                  size={25}
                  color="#D4AF37"
                  onPress={() => navigation.toggleDrawer()}
                />
              </TouchableOpacity>
            ),
            headerLeftContainerStyle: { paddingLeft: 10 },
          })}
        />
        <Stack.Screen name="Person Details" component={PersonDetailsScreen} />
        <Stack.Screen name="Film Details" component={FilmDetailsScreen} />
      </Stack.Navigator>
    );
  };

  const CustomDrawerContentComponent = props => {
    return (
      <View style={{ flex: 1 }}>
        <Text>Hi</Text>
        <DrawerItem
          label="People List"
          onPress={() => {
            props.navigation.navigate('People List');
          }}
        />

        <DrawerItem
          label="Film List"
          onPress={() => {
            props.navigation.navigate('Film List');
          }}
        />
      </View>
    );
  };

  const DrawerContainer = () => {
    return (
      <Drawer.Navigator>
        {/* drawerContent={props => <CustomDrawerContentComponent {...props} />}> */}
        <Drawer.Screen name="People" component={PeopleContainer} />
        <Drawer.Screen name="Films" component={FilmContainer} />
      </Drawer.Navigator>
    );
  };

  const StackContainer = () => {
    return (
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{ headerTitleAlign: 'center' }}>
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Signup" component={SignupScreen} />
        <Stack.Screen name="Home" component={DrawerContainer} />
      </Stack.Navigator>
    );
  };
  return (
    <Provider store={store}>
      <NavigationContainer>
        {isLogin ? <DrawerContainer /> : <StackContainer />}
      </NavigationContainer>
    </Provider>
  );
};

export default App;
